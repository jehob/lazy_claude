use reqwest;
use reqwest::header;
use serde::Deserialize;
use serde::Serialize;
use serde_json;

use crate::app::Prompt;
use crate::config;

#[derive(Debug, Deserialize)]
struct ApiResponse {
    content: Vec<Content>,
}

#[derive(Debug, Deserialize)]
struct Content {
    text: String,
}

pub async fn make_api_request(prompt: &Prompt) -> Option<String> {
    // Making a GET request
    let config = config::get_config();
    let url_api = "https://api.anthropic.com/v1/messages";

    let client = reqwest::Client::new();
    let body = serde_json::json!({
        "model": "claude-3-opus-20240229",
        "max_tokens": 1024,
        "messages": prompt.messages,
    });

    let response = client
        .post(url_api)
        .header("x-api-key", config.ANTHROPIC_API_KEY)
        .header("anthropic-version", "2023-06-01")
        .header("content-type", "application/json")
        .json(&body)
        .send()
        .await;

    match response {
        Ok(response) => {
            if response.status().is_success() {
                // Parsing the response body as JSON
                let api_response: ApiResponse = response.json().await.unwrap();

                api_response
                    .content
                    .first()
                    .map(|content| content.text.clone())
            } else {
                println!("Request failed with status: {}", response.status());
                println!(
                    "Detailed error information: {:#?}",
                    response.error_for_status()
                );
                None
            }
        }
        Err(err) => {
            println!("Request failed: {}", err);
            None
        }
    }
}
