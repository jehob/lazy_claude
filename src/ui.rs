use ratatui::{
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    text::{Line, Span, Text},
    widgets::{Block, Borders, Clear, Padding, Paragraph, Wrap},
    Frame,
};
use tui_textarea::TextArea;

use crate::app::{App, CurrentScreen, CurrentlyEditing};

pub fn ui(f: &mut Frame, app: &App, textarea: &TextArea) {
    // Create the layout sections.
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Length(3),
            Constraint::Min(1),
            Constraint::Length(3),
        ])
        .split(f.size());

    let title_block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default());

    let title = Paragraph::new(Text::styled("Prompt", Style::default().fg(Color::Green)))
        .block(title_block);

    f.render_widget(title, chunks[0]);

    // messages
    let message_chunk = chunks[1];
    let mut message_paragraphs = vec![];

    for message in &app.prompt.messages {
        let base = match message.role {
            crate::app::MessageSender::User => "User:",
            crate::app::MessageSender::Assistant => "Assistant:",
        };
        let text = format!("{}\n{}", base, message.content.clone());
        let paragraph = Paragraph::new(text).wrap(Wrap { trim: true }).block(
            Block::default()
                .borders(Borders::BOTTOM)
                .padding(Padding::horizontal(5)),
        );
        message_paragraphs.push(paragraph);
    }

    let mut constraints = vec![];
    for i in 0..message_paragraphs.len() {
        constraints.push(Constraint::Length(
            (app.prompt.messages[i].content.split('\n').count() + 2)
                .try_into()
                .unwrap(),
        ));
    }
    constraints.push(Constraint::Min(0));

    let message_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(constraints)
        .split(message_chunk);

    for (i, paragraph) in message_paragraphs.iter().enumerate() {
        f.render_widget(paragraph, message_layout[i]);
    }

    // let mut list_items = Vec::<ListItem>::new();
    //
    // for message in &app.prompt.messages {
    //     let paragraph = Paragraph::new(message.message.clone()).wrap(Wrap {trim: true});
    //     list_items.push(ListItem::new(paragraph));
    //     //list_items.push(ListItem::new(Paragraph::from(item.message)));
    //     // list_items.push(ListItem::new(Line::from(Span::styled(
    //     //     message.message.clone(),
    //     //     //format!("{}", message.message),
    //     //     Style::default().fg(Color::Yellow),
    //     // ))));
    // }
    // let list = List::new(list_items);
    //
    // f.render_widget(list, chunks[1]);

    let current_navigation_text = vec![
        // The first half of the text
        match app.current_screen {
            CurrentScreen::Main => Span::styled("Normal Mode", Style::default().fg(Color::Green)),
            CurrentScreen::Editing => {
                Span::styled("Editing Mode", Style::default().fg(Color::Yellow))
            }
            CurrentScreen::Exiting => Span::styled("Exiting", Style::default().fg(Color::LightRed)),
        }
        .to_owned(),
        // A white divider bar to separate the two sections
        Span::styled(" | ", Style::default().fg(Color::White)),
        // The final section of the text, with hints on what the user is editing
        {
            if let Some(editing) = &app.currently_editing {
                match editing {
                    CurrentlyEditing::Key => {
                        Span::styled("Editing Json Key", Style::default().fg(Color::Green))
                    }
                    CurrentlyEditing::Value => {
                        Span::styled("Editing Json Value", Style::default().fg(Color::LightGreen))
                    }
                }
            } else {
                Span::styled("Not Editing Anything", Style::default().fg(Color::DarkGray))
            }
        },
    ];

    let mode_footer = Paragraph::new(Line::from(current_navigation_text))
        .block(Block::default().borders(Borders::ALL));

    let current_keys_hint = {
        match app.current_screen {
            CurrentScreen::Main => Span::styled(
                "(q) to quit / (m) to create a new message",
                Style::default().fg(Color::Red),
            ),
            CurrentScreen::Editing => Span::styled(
                "(ESC) to cancel/(Tab) to switch boxes/enter to complete",
                Style::default().fg(Color::Red),
            ),
            CurrentScreen::Exiting => Span::styled(
                "(q) to quit / (e) to make new pair",
                Style::default().fg(Color::Red),
            ),
        }
    };

    let key_notes_footer =
        Paragraph::new(Line::from(current_keys_hint)).block(Block::default().borders(Borders::ALL));

    let footer_chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(50), Constraint::Percentage(50)])
        .split(chunks[2]);

    f.render_widget(mode_footer, footer_chunks[0]);
    f.render_widget(key_notes_footer, footer_chunks[1]);

    if let Some(editing) = &app.currently_editing {
        let area = centered_rect(60, 25, f.size());
        let block = Block::default()
            .title("New Message")
            .borders(Borders::ALL)
            .border_style(Style::default().fg(Color::White));

        f.render_widget(&block, area);
        f.render_widget(textarea.widget(), block.inner(area));
    }

    if let CurrentScreen::Exiting = app.current_screen {
        f.render_widget(Clear, f.size()); //this clears the entire screen and anything already drawn
        let popup_block = Block::default()
            .title("Y/N")
            .borders(Borders::NONE)
            .style(Style::default().bg(Color::DarkGray));

        let exit_text = Text::styled(
            "Would you like to output the buffer as json? (y/n)",
            Style::default().fg(Color::Red),
        );
        // the `trim: false` will stop the text from being cut off when over the edge of the block
        let exit_paragraph = Paragraph::new(exit_text)
            .block(popup_block)
            .wrap(Wrap { trim: false });

        let area = centered_rect(60, 25, f.size());
        f.render_widget(exit_paragraph, area);
    }
}

/// helper function to create a centered rect using up certain percentage of the available rect `r`
fn centered_rect(percent_x: u16, percent_y: u16, r: Rect) -> Rect {
    // Cut the given rectangle into three vertical pieces
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Percentage((100 - percent_y) / 2),
            Constraint::Percentage(percent_y),
            Constraint::Percentage((100 - percent_y) / 2),
        ])
        .split(r);

    // Then cut the middle vertical piece into three width-wise pieces
    Layout::default()
        .direction(Direction::Horizontal)
        .constraints([
            Constraint::Percentage((100 - percent_x) / 2),
            Constraint::Percentage(percent_x),
            Constraint::Percentage((100 - percent_x) / 2),
        ])
        .split(popup_layout[1])[1] // Return the middle chunk
}
