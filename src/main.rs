use std::{error::Error, io};

use crossterm::{
    event::{
        self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode, KeyEventKind, KeyModifiers,
    },
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::style::{Color, Modifier, Style};
use ratatui::{
    backend::{Backend, CrosstermBackend},
    Terminal,
};

mod app;
mod ui;

use crate::{
    app::{App, CurrentScreen, CurrentlyEditing},
    ui::ui,
};
use tui_textarea::TextArea;

mod api_communication;
mod config;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    enable_raw_mode()?;
    let mut stderr = io::stderr(); // This is a special case. Normally using stdout is fine
    execute!(stderr, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stderr);
    let mut terminal = Terminal::new(backend)?;

    // create app and run it
    let mut app = App::new();
    let res = run_app(&mut terminal, &mut app).await;

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{err:?}");
    }

    Ok(())
}

async fn run_app<B: Backend>(terminal: &mut Terminal<B>, app: &mut App) -> io::Result<bool> {
    let mut textarea = TextArea::default();

    loop {
        terminal.draw(|f| ui(f, app, &textarea))?;

        if let Event::Key(key) = event::read()? {
            if key.kind == event::KeyEventKind::Release {
                // Skip events that are not KeyEventKind::Press
                continue;
            }
            match app.current_screen {
                CurrentScreen::Main => match key.code {
                    KeyCode::Char('m') => {
                        app.current_screen = CurrentScreen::Editing;

                        textarea = TextArea::default();
                        textarea.set_line_number_style(Style::default());
                        let no_underline = Style::default().remove_modifier(Modifier::UNDERLINED);
                        textarea.set_cursor_line_style(no_underline);

                        app.currently_editing = Some(CurrentlyEditing::Key);
                    }
                    KeyCode::Char('q') => {
                        app.current_screen = CurrentScreen::Exiting;
                    }
                    _ => {}
                },
                CurrentScreen::Exiting => match key.code {
                    KeyCode::Char('y') => {
                        return Ok(true);
                    }
                    KeyCode::Char('n') | KeyCode::Char('q') => {
                        return Ok(false);
                    }
                    _ => {}
                },
                CurrentScreen::Editing if key.kind == KeyEventKind::Press => match key.code {
                    KeyCode::Enter => {
                        if key.modifiers.contains(KeyModifiers::SHIFT)
                            || key.modifiers.contains(KeyModifiers::ALT)
                            || key.modifiers.contains(KeyModifiers::CONTROL)
                        {
                            //send_message(&app.key_input);
                            //app.key_input.clear();

                            app.save_key_value();
                            app.save_message(textarea.lines().join("\n"), app::MessageSender::User);

                            app.current_screen = CurrentScreen::Main;

                            let res = crate::api_communication::make_api_request(&app.prompt).await;
                            app.save_message(res.unwrap(), app::MessageSender::Assistant);
                        } else {
                            textarea.input(key);
                            // Regular Enter key pressed
                            //app.key_input.push('\n');
                        }
                    }
                    // KeyCode::Backspace => {
                    //     if let Some(editing) = &app.currently_editing {
                    //         match editing {
                    //             CurrentlyEditing::Key => {
                    //                 app.key_input.pop();
                    //             }
                    //             CurrentlyEditing::Value => {
                    //                 app.value_input.pop();
                    //             }
                    //         }
                    //     }
                    // }
                    KeyCode::Esc => {
                        app.current_screen = CurrentScreen::Main;
                        app.currently_editing = None;
                    }
                    // KeyCode::Tab => {
                    //     app.toggle_editing();
                    // }
                    // KeyCode::Char(value) => {
                    //     if let Some(editing) = &app.currently_editing {
                    //         match editing {
                    //             CurrentlyEditing::Key => {
                    //                 app.key_input.push(value);
                    //             }
                    //             CurrentlyEditing::Value => {
                    //                 app.value_input.push(value);
                    //             }
                    //         }
                    //     }
                    // }
                    _ => {
                        textarea.input(key);
                    }
                },
                _ => {}
            }
        }
    }
}
