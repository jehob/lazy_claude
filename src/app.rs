use serde;
use serde::Serialize;
use std::collections::HashMap;

pub enum CurrentScreen {
    Main,
    Editing,
    Exiting,
}

pub enum CurrentlyEditing {
    Key,
    Value,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum MessageSender {
    User,
    Assistant,
}

#[derive(Debug, Serialize)]
pub struct Message {
    pub role: MessageSender,
    pub content: String,
}

pub struct Prompt {
    pub messages: Vec<Message>,
    pub name: String,
}

pub struct App {
    pub prompt: Prompt,
    pub current_message: String,
    pub current_screen: CurrentScreen, // the current screen the user is looking at, and will later determine what is rendered.
    pub key_input: String,             // the currently being edited json key.
    pub value_input: String,           // the currently being edited json value.
    pub pairs: HashMap<String, String>, // The representation of our key and value pairs with serde Serialize support
    pub currently_editing: Option<CurrentlyEditing>, // the optional state containing which of the key or value pair the user is editing. It is an option, because when the user is not directly editing a key-value pair, this will be set to `None`.
}

impl App {
    pub fn new() -> App {
        App {
            prompt: Prompt {
                messages: Vec::new(),
                name: String::new(),
            },
            current_message: String::new(),
            current_screen: CurrentScreen::Main,

            key_input: String::new(),
            value_input: String::new(),
            pairs: HashMap::new(),
            currently_editing: None,
        }
    }

    pub fn save_key_value(&mut self) {
        self.pairs
            .insert(self.key_input.clone(), self.value_input.clone());

        self.key_input = String::new();
        self.value_input = String::new();
        self.currently_editing = None;
    }

    pub fn save_message(&mut self, new_message: String, role: MessageSender) {
        self.prompt.messages.push(Message {
            content: new_message,
            role,
        });
    }
}
